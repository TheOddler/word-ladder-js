var dict = [
    "abla",
    "abone",
    "aç",
    "acaba",
    "acayip",
    "acele",
    "acemi",
    "acil",
    "acı",
    "açı",
    "açık",
    "açıklama",
    "acıklı",
    "acımasız",
    "açlık",
    "açma",
    "ad",
    "ada",
    "adalet",
    "adam",
    "aday",
    "adet",
    "adeta",
    "adil",
    "adım",
    "adres",
    "afet",
    "afiş",
    "afiyet",
    "ağ",
    "ağabey",
    "ağaç",
    "ağaçkakan",
    "ağır",
    "ağırlık",
    "ağız",
    "ağrı",
    "ağustos",
    "ahır",
    "ahşap",
    "ahtapot",
    "ahududu",
    "aile",
    "ait",
    "ak",
    "akarsu",
    "akbaba",
    "akciğer",
    "akıl",
    "akraba",
    "akrep",
    "akşam",
    "aksan",
    "aksiyon",
    "aktivite",
    "aktör",
    "akü",
    "akvaryum",
    "al",
    "alan",
    "alarm",
    "albüm",
    "alçak",
    "alçı",
    "alerji",
    "alet",
    "alev",
    "alfabe",
    "algı",
    "alın",
    "alışkanlık",
    "alıştırma",
    "alışveriş",
    "alkış",
    "allah",
    "alo",
    "alt",
    "altı",
    "altın",
    "altıncı",
    "altmış",
    "altyapı",
    "alüminyum",
    "ama",
    "amaç",
    "ambar",
    "ambulans",
    "amca",
    "ameliyat",
    "ampul",
    "an",
    "ana",
    "anahtar",
    "anaokulu",
    "anayasa",
    "ancak",
    "ani",
    "animasyon",
    "anı",
    "anıt",
    "anket",
    "anlam",
    "anlaşma",
    "anlayış",
    "anlayışlı",
    "anne",
    "anneanne",
    "anons",
    "ansiklopedi",
    "ant",
    "anten",
    "antik",
    "antika",
    "antrenör",
    "apartman",
    "ara",
    "araba",
    "arabesk",
    "araç",
    "aralık",
    "araştırma",
    "arı",
    "arka",
    "arkadaş",
    "arkadaşlık",
    "armağan",
    "armut",
    "arp",
    "arpa",
    "arsa",
    "artı",
    "arzu",
    "as",
    "aş",
    "asa",
    "aşağı",
    "asansör",
    "aşçı",
    "asi",
    "aşı",
    "aşık",
    "asıl",
    "aşk",
    "asker",
    "askı",
    "asla",
    "aslan",
    "asma",
    "astım",
    "astronot",
    "aşure",
    "at",
    "ata",
    "atari",
    "atasözü",
    "ateş",
    "atık",
    "atkı",
    "atlas",
    "atlet",
    "atlıkarınca",
    "atom",
    "av",
    "avcı",
    "avize",
    "avlu",
    "avuç",
    "avukat",
    "avunç",
    "ay",
    "ayak",
    "ayakkabı",
    "ayçiçeği",
    "ayı",
    "aylık",
    "ayna",
    "aynı",
    "ayraç",
    "ayran",
    "ayrı",
    "ayrıntı",
    "ayva",
    "az",
    "azılı",
    "baba",
    "babaanne",
    "baca",
    "bacak",
    "badem",
    "bağ",
    "bagaj",
    "bağımsızlık",
    "bağlama",
    "bahar",
    "baharat",
    "bahçe",
    "bahçıvan",
    "bakan",
    "bakir",
    "bakım",
    "bakır",
    "baklava",
    "bal",
    "bale",
    "balina",
    "balık",
    "balıkçı",
    "balkon",
    "balo",
    "balon",
    "balta",
    "bambu",
    "bamya",
    "banjo",
    "bank",
    "banka",
    "bant",
    "banyo",
    "baraj",
    "bardak",
    "barınak",
    "barış",
    "bas",
    "baş",
    "başak",
    "başarı",
    "başarılı",
    "başbakan",
    "başka",
    "başkan",
    "başkent",
    "basket",
    "basketbol",
    "baskı",
    "başlık",
    "başparmak",
    "baston",
    "bat",
    "batı",
    "battaniye",
    "bavul",
    "bay",
    "bayan",
    "bayat",
    "baykuş",
    "bayrak",
    "bayram",
    "bazen",
    "bebek",
    "becerikli",
    "bedava",
    "beden",
    "bej",
    "bekçi",
    "bel",
    "bela",
    "belediye",
    "belge",
    "ben",
    "bencil",
    "benzer",
    "benzin",
    "berber",
    "bere",
    "berrak",
    "beş",
    "beşik",
    "beşinci",
    "beste",
    "beton",
    "bey",
    "beyaz",
    "beyefendi",
    "beyin",
    "bez",
    "bezelye",
    "biber",
    "biblo",
    "biçim",
    "bilardo",
    "bilek",
    "bilet",
    "bilezik",
    "bilgi",
    "bilgili",
    "bilgisayar",
    "bilim",
    "bilinç",
    "bilinçli",
    "bilmece",
    "bilye",
    "bin",
    "bina",
    "bir",
    "biraz",
    "biricik",
    "birinci",
    "birlik",
    "bisiklet",
    "bisküvi",
    "bitki",
    "biz",
    "bıçak",
    "bıyık",
    "bluz",
    "boa",
    "böbrek",
    "böcek",
    "bodrum",
    "boğa",
    "boğaz",
    "boks",
    "boksör",
    "bol",
    "bölge",
    "bölüm",
    "bomba",
    "boncuk",
    "borç",
    "bordo",
    "börek",
    "boru",
    "boş",
    "bot",
    "boy",
    "boya",
    "boyacı",
    "böyle",
    "boynuz",
    "boyun",
    "boz",
    "bozuk",
    "bronz",
    "buçuk",
    "büfe",
    "buğday",
    "bugün",
    "buhar",
    "bük",
    "buket",
    "bukle",
    "bul",
    "bulaşık",
    "bülbül",
    "bulgu",
    "bulgur",
    "bulut",
    "bulutlu",
    "burç",
    "burs",
    "burun",
    "bütün",
    "büyü",
    "büyük",
    "buz",
    "buzağı",
    "buzdolabı",
    "çaba",
    "çabuk",
    "cacık",
    "cadde",
    "cadı",
    "çadır",
    "çağ",
    "çağdaş",
    "çak",
    "çakal",
    "çakıl",
    "çal",
    "çalarsaat",
    "çalgı",
    "çalı",
    "cam",
    "çam",
    "çamaşır",
    "cambaz",
    "çamur",
    "can",
    "çan",
    "çanak",
    "canavar",
    "cankurtaran",
    "canlı",
    "çanta",
    "çap",
    "çapa",
    "çapak",
    "çar",
    "çare",
    "çaresiz",
    "çark",
    "çarpı",
    "çarşaf",
    "çarşamba",
    "çarşı",
    "casus",
    "çatal",
    "çatı",
    "çavuş",
    "çay",
    "çaycı",
    "çaydanlık",
    "çayır",
    "caz",
    "çek",
    "ceket",
    "çekiç",
    "çekirdek",
    "çekirge",
    "çekmece",
    "çekyat",
    "çelik",
    "çello",
    "çember",
    "çene",
    "çengel",
    "cennet",
    "cep",
    "çerçeve",
    "çerez",
    "cesaret",
    "çeşit",
    "çeşitli",
    "çeşme",
    "çeşni",
    "cesur",
    "çete",
    "cetvel",
    "cevap",
    "ceviz",
    "çevre",
    "ceylan",
    "çeyrek",
    "ceza",
    "cezve",
    "çiçek",
    "cici",
    "ciddi",
    "çift",
    "çiftçi",
    "çiftlik",
    "çiğ",
    "çiğdem",
    "ciğer",
    "çikolata",
    "çil",
    "cila",
    "çile",
    "çilek",
    "cilt",
    "çim",
    "çimen",
    "çimento",
    "cimri",
    "cip",
    "çip",
    "cips",
    "cisim",
    "çit",
    "çita",
    "civciv",
    "çivi",
    "çiy",
    "çizgi",
    "çizgifilm",
    "çizme",
    "çığ",
    "çığlık",
    "çıkış",
    "çılgın",
    "çınar",
    "çıngırak",
    "çıplak",
    "cıva",
    "çoban",
    "çocuk",
    "çocukluk",
    "çoğul",
    "çoğunluk",
    "çok",
    "çöl",
    "çomak",
    "cömert",
    "çömlek",
    "çöp",
    "çöpçü",
    "çorap",
    "çorba",
    "çörek",
    "coşku",
    "çöz",
    "çözüm",
    "çubuk",
    "cüce",
    "çukur",
    "cuma",
    "cumartesi",
    "cumhuriyet",
    "cümle",
    "çünkü",
    "çupra",
    "çürük",
    "çuval",
    "cüzdan",
    "dadı",
    "dağ",
    "daha",
    "daima",
    "dair",
    "daire",
    "dakik",
    "dakika",
    "dal",
    "dalga",
    "dam(çatı)",
    "dama",
    "damak",
    "damar",
    "damat",
    "damga",
    "damla",
    "dana",
    "dans",
    "dar",
    "dava",
    "davet",
    "davranış",
    "davul",
    "dayı",
    "dede",
    "defa",
    "defile",
    "defne",
    "defter",
    "değer",
    "değil",
    "değirmen",
    "değişiklik",
    "değnek",
    "deha",
    "dekor",
    "delgi",
    "delik",
    "delikanlı",
    "delil",
    "dem",
    "demet",
    "demir",
    "demiryolu",
    "demokrasi",
    "deney",
    "deneyimli",
    "denge",
    "deniz",
    "denizaltı",
    "denizanası",
    "denizci",
    "denizyıldızı",
    "depo",
    "deprem",
    "dere",
    "derece",
    "dergi",
    "deri",
    "derin",
    "dernek",
    "ders",
    "dershane",
    "dert",
    "derya",
    "deste",
    "destek",
    "detay",
    "dev",
    "devam",
    "deve",
    "devlet",
    "deyim",
    "diğer",
    "dik",
    "dikdörtgen",
    "diken",
    "dikey",
    "dikiş",
    "dikkat",
    "dil",
    "dil(lisan)",
    "dilek",
    "dilim",
    "dinç",
    "dinozor",
    "dip",
    "direk",
    "direksiyon",
    "diri",
    "dirsek",
    "diş",
    "dişçi",
    "dişi",
    "disk",
    "disko",
    "diz",
    "dize",
    "dizi",
    "dış",
    "do",
    "doğa",
    "doğal",
    "doğru",
    "doğu",
    "doğum",
    "doksan",
    "doktor",
    "doku",
    "dokuz",
    "dolap",
    "dolma",
    "dolu",
    "domates",
    "domuz",
    "dondurma",
    "dönem",
    "döner",
    "dönüş",
    "dört",
    "dost",
    "dostluk",
    "dosya",
    "drama",
    "dudak",
    "düdük",
    "düet",
    "düğme",
    "düğün",
    "duman",
    "dümen",
    "dün",
    "dünya",
    "durak",
    "dürbün",
    "duru",
    "durum",
    "dürüm",
    "dürüst",
    "duş",
    "düş",
    "düşünce",
    "dut",
    "duvar",
    "duy",
    "duyarlı",
    "duygu",
    "düz",
    "düzen",
    "düzey",
    "ebe",
    "ebru",
    "eczacı",
    "eczane",
    "edebiyat",
    "efendi",
    "efsane",
    "eğe",
    "eğer",
    "eğik",
    "eğitim",
    "eğitimli",
    "eğlence",
    "eğlenceli",
    "ego",
    "eğri",
    "ejderha",
    "ek",
    "ekim",
    "ekip",
    "eklem",
    "ekler",
    "ekmek",
    "ekose",
    "ekran",
    "eksi",
    "ekşi",
    "eksik",
    "el",
    "ela",
    "elbise",
    "elçi",
    "eldiven",
    "elektrik",
    "elips",
    "elk",
    "elli",
    "elma",
    "elmas",
    "emek",
    "emekli",
    "emir",
    "emlak",
    "emzik",
    "enerji",
    "engel",
    "enişte",
    "ense",
    "enstrüman",
    "er",
    "erik",
    "erkek",
    "erken",
    "eş",
    "eşarp",
    "esas",
    "eşek",
    "eser",
    "eski",
    "esmer",
    "esnaf",
    "eşofman",
    "espri",
    "eşya",
    "et",
    "etek",
    "etiket",
    "etki",
    "etraf",
    "ev",
    "evcil",
    "evcilik",
    "evet",
    "evlat",
    "evli",
    "evren",
    "eylem",
    "eylül",
    "ezme",
    "fa",
    "fabl",
    "fabrika",
    "fal",
    "far",
    "fare",
    "fark",
    "fasulye",
    "fatura",
    "fay",
    "fayda",
    "fazla",
    "felaket",
    "felsefe",
    "fener",
    "ferah",
    "feribot",
    "fermuar",
    "fes",
    "fidan",
    "fiil",
    "fikir",
    "fil",
    "file",
    "filiz",
    "film",
    "fincan",
    "fiş",
    "fişek",
    "fiyat",
    "fizik",
    "fıçı",
    "fıkra",
    "fındık",
    "fırça",
    "fırın",
    "fırıncı",
    "fırka",
    "fırsat",
    "fırtına",
    "fıstık",
    "flaş",
    "flüt",
    "fok",
    "fön",
    "fosil",
    "fotoğraf",
    "fotokopi",
    "fragman",
    "fren",
    "futbol",
    "futbolcu",
    "füze",
    "gaga",
    "galaksi",
    "galeri",
    "galiba",
    "galibiyet",
    "galip",
    "gam",
    "gamze",
    "gani",
    "gar",
    "garaj",
    "garanti",
    "garip",
    "garson",
    "gaz",
    "gazete",
    "gazeteci",
    "gazoz",
    "geç",
    "gecelik",
    "geçit",
    "gelecek",
    "gelin",
    "gelincik",
    "gelir",
    "gemi",
    "genç",
    "gençlik",
    "genel",
    "geniş",
    "gerçek",
    "gerdanlık",
    "gereçleri",
    "gerek",
    "gergedan",
    "geri",
    "gevrek(cips)",
    "geyik",
    "gez",
    "gezegen",
    "gezi",
    "gibi",
    "giriş",
    "gişe",
    "gitar",
    "giyim",
    "giysi",
    "gizem",
    "gıcık",
    "gıda",
    "göbek",
    "göç",
    "göğüs",
    "gök",
    "gökkuşağı",
    "gökyüzü",
    "gol",
    "göl",
    "gölcük",
    "gölet",
    "gölge",
    "gömlek",
    "gonca",
    "gönül",
    "gönüllü",
    "göre",
    "görev",
    "görevli",
    "goril",
    "görüş",
    "gösteri",
    "gövde",
    "göz",
    "gözlük",
    "gözyaşı",
    "grafik",
    "gram",
    "greyfurt",
    "gri",
    "grup",
    "güç",
    "güçlü",
    "guguk",
    "gül",
    "gülünç",
    "gümrük",
    "gümüş",
    "gün",
    "günaydın",
    "gündem",
    "güneş",
    "güneşli",
    "güney",
    "günlük",
    "gür",
    "güreş",
    "gürültü",
    "gurur",
    "gururlu",
    "güveç",
    "güven",
    "güvenlik",
    "güvercin",
    "güz",
    "güzel",
    "güzellik",
    "haber",
    "hafif",
    "hafıza",
    "hafta",
    "hak",
    "hakem",
    "hakim",
    "hala",
    "halat",
    "halay",
    "halı",
    "halk",
    "halka",
    "halter",
    "hamak",
    "hamam",
    "hamburger",
    "hamile",
    "hamle",
    "hamsi",
    "hamur",
    "hangi",
    "hanım",
    "hap",
    "hapşu",
    "hareket",
    "harf",
    "hariç",
    "harika",
    "harita",
    "harp",
    "hasar",
    "hasta",
    "hastalık",
    "hastane",
    "hat",
    "hata",
    "hatıra",
    "hava",
    "havaalanı",
    "havlu",
    "havuç",
    "havuz",
    "hayal",
    "hayalet",
    "hayat",
    "hayır",
    "hayran",
    "hayret",
    "haysiyet",
    "hayvan",
    "hayvanat",
    "hayvancılık",
    "hayvansal",
    "hazar",
    "hazin",
    "hazine",
    "haziran",
    "hazır",
    "hazırlık",
    "hece",
    "hedef",
    "hediye",
    "helikopter",
    "helva",
    "hemen",
    "hemşire",
    "hentbol",
    "henüz",
    "hep",
    "hepsi",
    "her",
    "herkes",
    "hesap",
    "heyecan",
    "heyecanlı",
    "heykel",
    "hiç",
    "hikaye",
    "hikaye",
    "hile",
    "hindi",
    "his",
    "hizmet",
    "hırka",
    "hırs",
    "hırsız",
    "hız",
    "hızlı",
    "hobi",
    "hödük",
    "hokey",
    "horoz",
    "hortum",
    "hoş",
    "hostes",
    "hücre",
    "hukuk",
    "hurma",
    "huy",
    "huzur",
    "iç",
    "içecek",
    "için",
    "içki",
    "idare",
    "iddia",
    "ifade",
    "iğne",
    "ihtiyaç",
    "iki",
    "ikiz",
    "iklim",
    "ikram",
    "iksir",
    "il",
    "ilaç",
    "ilan",
    "ilçe",
    "ileri",
    "ilgi",
    "ilham",
    "ilişki",
    "ilk",
    "ilkbahar",
    "ilke",
    "ilköğretim",
    "ilkokul",
    "imza",
    "in",
    "inanç",
    "inatçı",
    "ince",
    "inci",
    "incir",
    "indirim",
    "inek",
    "inşaat",
    "insan",
    "insanlık",
    "insanoğlu",
    "internet",
    "ip",
    "ipek",
    "iplik",
    "iptal",
    "ipucu",
    "iri",
    "irmik",
    "is",
    "iş",
    "işaret",
    "işçi",
    "isim",
    "iskele",
    "iskelet",
    "iskemle",
    "işlem",
    "istasyon",
    "istek",
    "işyeri",
    "it",
    "itfaiye",
    "itiraz",
    "iyi",
    "iyilik",
    "iz",
    "izci",
    "izin",
    "ıhlamur",
    "ılık",
    "ırk",
    "ırmak",
    "ısı",
    "ışık",
    "ıslak",
    "ıslık",
    "ıspanak",
    "ıssız",
    "ıstakoz",
    "ızgara",
    "jet",
    "jeton",
    "joker",
    "jokey",
    "jöle",
    "judo",
    "kabak",
    "kaban",
    "kabin",
    "kablo",
    "kabuk",
    "kabul",
    "kabus",
    "kaç",
    "kadife",
    "kadın",
    "kafa",
    "kafe",
    "kafes",
    "kağıt",
    "kahkaha",
    "kahraman",
    "kahvaltı",
    "kahve",
    "kahverengi",
    "kakao",
    "kal",
    "kalabalık",
    "kalbur",
    "kalça",
    "kaldırım",
    "kale",
    "kaleci",
    "kalem",
    "kalemtıraş",
    "kalite",
    "kalın",
    "kalıp",
    "kalkan",
    "kalp",
    "kamera",
    "kamış",
    "kamp",
    "kampüs",
    "kamyon",
    "kamyonet",
    "kan",
    "kanal",
    "kanarya",
    "kanat",
    "kanca",
    "kanepe",
    "kanguru",
    "kanıt",
    "kanka",
    "kanun",
    "kap",
    "kapak",
    "kapı",
    "kaplan",
    "kaplumbağa",
    "kaptan",
    "kar",
    "kara",
    "karaciğer",
    "karanfil",
    "kararlı",
    "karatahta",
    "karayolu",
    "kardeş",
    "kare",
    "karga",
    "kargo",
    "kariyer",
    "karizma",
    "karizmatik",
    "karın",
    "karınca",
    "karış",
    "karma",
    "karmaşa",
    "karnabahar",
    "karne",
    "karpuz",
    "karşı",
    "karşılık",
    "kart",
    "kartal",
    "karton",
    "kas",
    "kaş",
    "kasa",
    "kasaba",
    "kasap",
    "kaşar",
    "kase",
    "kase",
    "kaset",
    "kasiyer",
    "kasık",
    "kaşık",
    "kasım",
    "kasırga",
    "kask",
    "kat",
    "katalog",
    "katsayı",
    "kavak",
    "kavanoz",
    "kavga",
    "kavi",
    "kavram",
    "kavşak",
    "kavun",
    "kay",
    "kaya",
    "kayak",
    "kaydırak",
    "kaygan",
    "kaygı",
    "kayık",
    "kayısı",
    "kaykay",
    "kaymak",
    "kaynak",
    "kaz",
    "kaza",
    "kazak",
    "kazan",
    "kebap",
    "keçi",
    "kedi",
    "kefal",
    "kehanet",
    "kek",
    "kekik",
    "kel",
    "kelebek",
    "kelime",
    "kelimeler",
    "keman",
    "kemer",
    "kemik",
    "kenar",
    "kendi",
    "kent",
    "kepçe",
    "kepek",
    "kere",
    "kereviz",
    "kertenkele",
    "kervan",
    "kes",
    "kese",
    "kesin",
    "keşke",
    "keskin",
    "kestane",
    "ketçap",
    "keten",
    "keyfi",
    "kez",
    "keza",
    "kibar",
    "kibrit",
    "kilim",
    "kilise",
    "kilit",
    "kilo",
    "kilogram",
    "kilometre",
    "kim",
    "kimlik",
    "kimse",
    "kimya",
    "kir",
    "kira",
    "kiraz",
    "kireç",
    "kirli",
    "kirpi",
    "kirpik",
    "kişi",
    "kişilik",
    "kitap",
    "kitaplık",
    "kivi",
    "kıdem",
    "kıl",
    "kılavuz",
    "kılıç",
    "kır",
    "kırk",
    "kırmızı",
    "kış",
    "kısa",
    "kısım",
    "kısır",
    "kıskanç",
    "kıta",
    "kıyafet",
    "kıyı",
    "kıyma",
    "kıymet",
    "kız",
    "kızak",
    "kızartma",
    "kızgın",
    "kızıl",
    "kızkardeş",
    "klasik",
    "klavye",
    "klima",
    "koala",
    "koç",
    "köfte",
    "kök",
    "koku",
    "kol",
    "kolaj",
    "kolay",
    "kolej",
    "koli",
    "kolon",
    "kolonya",
    "koltuk",
    "kolye",
    "komedi",
    "komik",
    "komşu",
    "kömür",
    "konak",
    "konser",
    "kontrol",
    "konu",
    "konuk",
    "konuşma",
    "köpek",
    "köprü",
    "köpük",
    "kor",
    "kör",
    "körebe",
    "körfez",
    "koridor",
    "korkak",
    "korku",
    "korkuluk",
    "korkunç",
    "korna",
    "koro",
    "korsan",
    "köşe",
    "köşk",
    "koskoca",
    "köstebek",
    "kostüm",
    "koşu",
    "koşul",
    "kot",
    "kota",
    "koton",
    "kötü",
    "kötülük",
    "kov",
    "kova",
    "kovan",
    "kovboy",
    "koy",
    "köy",
    "köylü",
    "koyu",
    "koyun",
    "koz",
    "köz",
    "koza",
    "kozalak",
    "kral",
    "kraliçe",
    "kravat",
    "krem",
    "krema",
    "krep",
    "kreş",
    "kuaför",
    "kucak",
    "küçük",
    "küf",
    "kuğu",
    "kukla",
    "kül",
    "kulaç",
    "külah",
    "kulak",
    "kule",
    "kültür",
    "kulübe",
    "kulüp",
    "kum",
    "kumanda",
    "kumaş",
    "kumbara",
    "küme",
    "kümes",
    "kumsal",
    "kunduz",
    "küp",
    "kupa",
    "küpe",
    "kupon",
    "kurabiye",
    "kural",
    "kurbağa",
    "kurban",
    "kurdele",
    "küre",
    "kürek",
    "kürk",
    "kurnaz",
    "kurs",
    "kürsü",
    "kurt",
    "kuru",
    "kurul",
    "kurum",
    "kuruş",
    "kurye",
    "kuş",
    "kuşak",
    "kuşku",
    "kusur",
    "kutsal",
    "kutu",
    "kütük",
    "kütüphane",
    "küvet",
    "kuvvet",
    "kuvvetli",
    "kuyruk",
    "kuyu",
    "kuyumcu",
    "kuzen",
    "kuzey",
    "kuzu",
    "la",
    "lacivert",
    "laf",
    "lahana",
    "lale",
    "lama",
    "lamba",
    "lastik",
    "lava",
    "lavabo",
    "lazım",
    "leke",
    "leopar",
    "leylek",
    "leziz",
    "lider",
    "lif",
    "lig",
    "liman",
    "limon",
    "limonata",
    "linç",
    "lira",
    "lise",
    "liste",
    "litre",
    "logo",
    "lokanta",
    "lokma",
    "lokomotif",
    "lokum",
    "lüks",
    "lunapark",
    "lütfen",
    "maaş",
    "maç",
    "macera",
    "madalya",
    "madde",
    "maden",
    "mağara",
    "mağaza",
    "magazin",
    "mahalle",
    "mahkeme",
    "makale",
    "makarna",
    "makas",
    "maket",
    "makine",
    "makinist",
    "makyaj",
    "mal",
    "malzeme",
    "mama",
    "mamut",
    "manav",
    "manda",
    "mandalina",
    "mangal",
    "mango",
    "mantar",
    "mantı",
    "manto",
    "manzara",
    "marina",
    "marka",
    "market",
    "marmelat",
    "mart",
    "martı",
    "marul",
    "masa",
    "maşa",
    "masal",
    "maske",
    "matematik",
    "matkap",
    "mavi",
    "maya",
    "maydanoz",
    "mayın",
    "mayıs",
    "maymun",
    "mayo",
    "mazi",
    "meclis",
    "medya",
    "meğer",
    "mekan",
    "mekik",
    "mektup",
    "melek",
    "melodi",
    "memleket",
    "memnun",
    "memur",
    "mendil",
    "menekşe",
    "menü",
    "merak",
    "meraklı",
    "mercek",
    "mercimek",
    "merdiven",
    "merhaba",
    "merhem",
    "merkez",
    "mesaj",
    "meşe",
    "mesele",
    "meslek",
    "metal",
    "metre",
    "metro",
    "mevcut",
    "mevsim",
    "meydan",
    "meyve",
    "meze",
    "mezun",
    "mi",
    "mide",
    "midye",
    "mikrofon",
    "mikrop",
    "mikroskop",
    "miktar",
    "millet",
    "milletvekili",
    "milyon",
    "mimar",
    "minder",
    "minibüs",
    "minik",
    "mis",
    "misafir",
    "misket",
    "miyav",
    "mıknatıs",
    "mısır",
    "mobilya",
    "moda",
    "model",
    "modern",
    "mola",
    "mont",
    "mor",
    "motor",
    "motosiklet",
    "mozaik",
    "mücadele",
    "mucit",
    "mucize",
    "mudi",
    "müdür",
    "muhabbet",
    "muhabir",
    "muhallebi",
    "mühendis",
    "muhtar",
    "müjde",
    "mülk",
    "mum",
    "mumya",
    "mürekkep",
    "muska",
    "musluk",
    "müşteri",
    "mutfak",
    "mutlu",
    "mutluluk",
    "mutsuz",
    "muz",
    "müze",
    "müzik",
    "müzikal",
    "müzisyen",
    "nal",
    "nane",
    "nar",
    "narin",
    "nasır",
    "naz",
    "nazar",
    "nazik",
    "ne",
    "neden",
    "nefes",
    "nehir",
    "nem",
    "neşe",
    "nesil",
    "nesne",
    "niçin",
    "nihayet",
    "nilüfer",
    "nine",
    "ninni",
    "nisan",
    "nişanlı",
    "nitelik",
    "niye",
    "niyet",
    "nöbet",
    "nohut",
    "nokta",
    "normal",
    "nöron",
    "not",
    "nota",
    "noter",
    "nüfus",
    "numara",
    "ocak",
    "öcü",
    "oda",
    "ödev",
    "ödül",
    "odun",
    "oduncu",
    "ofis",
    "öfke",
    "öfkeli",
    "oğlan",
    "öğle",
    "öğlen",
    "öğrenci",
    "öğretim",
    "öğretmen",
    "oğul",
    "oje",
    "ok",
    "okey",
    "oksijen",
    "öksürük",
    "okul",
    "okuma",
    "öküz",
    "okyanus",
    "olanak",
    "olay",
    "ölçü",
    "olta",
    "ölüm",
    "olumlu",
    "olumsuz",
    "omlet",
    "ömür",
    "omurga",
    "omuz",
    "on",
    "ön",
    "önem",
    "öneri",
    "önlük",
    "onuncu",
    "onur",
    "opera",
    "öpücük",
    "ördek",
    "ordu",
    "örf",
    "org",
    "organ",
    "organik",
    "örgü",
    "orkestra",
    "orman",
    "örnek",
    "orta",
    "ortak",
    "ortalama",
    "ortam",
    "örtü",
    "örümcek",
    "ot",
    "öte",
    "otel",
    "otobüs",
    "otomobil",
    "otopark",
    "otoyol",
    "oturak",
    "otuz",
    "ova",
    "oval",
    "övgü",
    "oy",
    "oy(rey)",
    "öykü",
    "oyun",
    "oyuncak",
    "oyuncu",
    "öz",
    "özel",
    "özellik",
    "özen",
    "özet",
    "özgür",
    "özgürlük",
    "özlem",
    "özür",
    "pabuç",
    "padişah",
    "pahalı",
    "paket",
    "palmiye",
    "palto",
    "palyaço",
    "pamuk",
    "pancar",
    "panda",
    "panel",
    "panik",
    "pankart",
    "pano",
    "pansiyon",
    "pantolon",
    "papa",
    "papağan",
    "papatya",
    "papaz",
    "para",
    "paragraf",
    "paralel",
    "paraşüt",
    "parça",
    "parfüm",
    "park",
    "parlak",
    "parmak",
    "parti",
    "paşa",
    "pasaport",
    "paspas",
    "pasta",
    "pastane",
    "pastel",
    "patates",
    "paten",
    "pati",
    "patlak",
    "patlıcan",
    "patron",
    "pay",
    "pazar",
    "pazartesi",
    "peçe",
    "peçete",
    "peki",
    "pembe",
    "pencere",
    "penguen",
    "pense",
    "perde",
    "peri",
    "peron",
    "perşembe",
    "petrol",
    "peynir",
    "pide",
    "pijama",
    "pike",
    "piknik",
    "pil",
    "pilav",
    "piliç",
    "pilot",
    "pipet",
    "piramit",
    "pire",
    "pirinç",
    "pis",
    "pişman",
    "pist",
    "pişti",
    "piyango",
    "piyano",
    "piyaz",
    "piyes",
    "piyon",
    "pizza",
    "pınar",
    "pırasa",
    "plaj",
    "plak",
    "plaka",
    "plan",
    "plastik",
    "plato",
    "polis",
    "pop",
    "portakal",
    "poşet",
    "posta",
    "postacı",
    "postane",
    "pota",
    "poz",
    "prens",
    "prenses",
    "priz",
    "problem",
    "profesör",
    "program",
    "proje",
    "prova",
    "puan",
    "pudra",
    "pul",
    "puma",
    "püre",
    "pusu",
    "pusula",
    "radyo",
    "raf",
    "rahat",
    "rakam",
    "raket",
    "rakip",
    "rakun",
    "randevu",
    "ranza",
    "rapor",
    "ray",
    "re",
    "reçel",
    "reçete",
    "refleks",
    "rehber",
    "rekabet",
    "reklam",
    "rende",
    "renk",
    "renkli",
    "resim",
    "ressam",
    "restoran",
    "rica",
    "ritim",
    "robot",
    "roka",
    "roket",
    "rol",
    "roman",
    "rozet",
    "ruh",
    "ruj",
    "rulo",
    "rüya",
    "rüzgar",
    "rüzgarlı",
    "saat",
    "sabah",
    "sabırlı",
    "sabun",
    "saç",
    "sade",
    "saf",
    "sağ",
    "sağır",
    "sağlam",
    "sağlık",
    "şah",
    "saha",
    "şahane",
    "sahil",
    "şahin",
    "sahip",
    "sahne",
    "sahte",
    "şair",
    "şaka",
    "sakal",
    "sakar",
    "sakat",
    "sakin",
    "sakız",
    "saklambaç",
    "saksı",
    "sal",
    "şal",
    "salam",
    "salata",
    "salatalık",
    "salça",
    "saldırı",
    "salep",
    "salı",
    "salıncak",
    "salkım",
    "salon",
    "salyangoz",
    "saman",
    "samimi",
    "şampiyon",
    "şampuan",
    "samur",
    "sanat",
    "sanatçı",
    "sanayi",
    "sandal",
    "sandalet",
    "sandalye",
    "sandık",
    "sandviç",
    "saniye",
    "şans",
    "sapan",
    "şapka",
    "saray",
    "sargı",
    "sarhoş",
    "sarı",
    "sarımsak",
    "sarışın",
    "şarj",
    "şarkı",
    "şarkıcı",
    "sarma",
    "şart",
    "şaşkın",
    "satıcı",
    "satır",
    "satış",
    "şato",
    "satranç",
    "say",
    "saydam",
    "sayfa",
    "saygı",
    "saygılı",
    "sayı",
    "sayın",
    "saz",
    "sebep",
    "sebze",
    "seçenek",
    "seçim",
    "şef",
    "sefer",
    "şeffaf",
    "şefkat",
    "şeftali",
    "şehir",
    "sehpa",
    "sek",
    "şeker",
    "şekil",
    "sekiz",
    "sekreter",
    "seksek",
    "seksen",
    "sel",
    "şelale",
    "selam",
    "şema",
    "şempanze",
    "şemsiye",
    "semt",
    "sen",
    "şen",
    "senaryo",
    "sene",
    "sepet",
    "sera",
    "seramik",
    "serbest",
    "serçe",
    "sergi",
    "seri",
    "serin",
    "sert",
    "serüven",
    "servis",
    "ses",
    "sessiz",
    "sessizlik",
    "set",
    "sevda",
    "sevecen",
    "sevgi",
    "sevgili",
    "sevimli",
    "sevinç",
    "seviye",
    "şey",
    "seyahat",
    "seyek",
    "seyirci",
    "şezlong",
    "si",
    "sicim",
    "şifre",
    "sihirbaz",
    "sihirli",
    "şiir",
    "şikayet",
    "silgi",
    "silindir",
    "sim",
    "sima",
    "şimdi",
    "simit",
    "şimşek",
    "sincap",
    "sinek",
    "sinema",
    "sinir",
    "siren",
    "şirin",
    "sirk",
    "sirke",
    "şirket",
    "sis",
    "şiş",
    "şişe",
    "sistem",
    "site",
    "sivri",
    "sivrisinek",
    "siyah",
    "siz",
    "sıcak",
    "sıcaklık",
    "sıfat",
    "sıfır",
    "sığ",
    "sığır",
    "sık",
    "şık",
    "sıkıcı",
    "sıkıntı",
    "şımarık",
    "sınav",
    "şınav",
    "sınıf",
    "sınır",
    "sıpa",
    "sır",
    "sıra",
    "sırt",
    "sırtçantası",
    "sıvı",
    "smaç",
    "soba",
    "sobe",
    "soda",
    "sofa",
    "şoför",
    "sofra",
    "soğan",
    "soğuk",
    "söğüt",
    "sohbet",
    "şok",
    "sokak",
    "sol",
    "solak",
    "şölen",
    "solucan",
    "soluk",
    "şömine",
    "somon",
    "son",
    "sonbahar",
    "sonra",
    "sonuç",
    "sopa",
    "sörf",
    "şort",
    "soru",
    "sorumlu",
    "sorumluluk",
    "sorun",
    "sos",
    "sosis",
    "şov",
    "şövalye",
    "soy",
    "soyadı",
    "şöyle",
    "soyut",
    "söz",
    "sözcük",
    "sözlük",
    "spor",
    "sporcu",
    "stadyum",
    "staj",
    "stat",
    "stres",
    "su",
    "şu",
    "şubat",
    "şube",
    "suç",
    "sucuk",
    "sufle",
    "sultan",
    "sumak",
    "sünger",
    "süper",
    "süpermarket",
    "şüphe",
    "süpürge",
    "sürahi",
    "surat",
    "süre",
    "sürpriz",
    "sürü",
    "sürücü",
    "şurup",
    "süs",
    "susam",
    "suşi",
    "suskun",
    "süslü",
    "süt",
    "tabak",
    "taban",
    "tabanca",
    "tabela",
    "tabiat",
    "tablo",
    "tabure",
    "taç",
    "tahin",
    "tahıl",
    "tahmin",
    "taht",
    "tahta",
    "tak",
    "takdir",
    "takı",
    "takım",
    "takla",
    "taksi",
    "takvim",
    "tam",
    "tamam",
    "tamirci",
    "tane",
    "tanı",
    "tanıdık",
    "tank",
    "tapınak",
    "taraf",
    "tarak",
    "tarih",
    "tarım",
    "tarla",
    "tartışma",
    "tarz",
    "tas",
    "taş",
    "tasarım",
    "taşıt",
    "tasma",
    "tatil",
    "tatlı",
    "tava",
    "tavan",
    "tavla",
    "tavşan",
    "tavuk",
    "tay",
    "tayt",
    "taze",
    "tazı",
    "tebeşir",
    "tef",
    "tehlike",
    "tek",
    "teker",
    "tekerlek",
    "tekir",
    "teklif",
    "tekme",
    "tekne",
    "teknik",
    "teknoloji",
    "tekrar",
    "tel",
    "telefon",
    "televizyon",
    "temel",
    "temiz",
    "temizlik",
    "temmuz",
    "temsilci",
    "ten",
    "tencere",
    "teneffüs",
    "tenis",
    "tepe",
    "tepki",
    "tepsi",
    "ter",
    "teras",
    "terazi",
    "terbiyeli",
    "terbiyesiz",
    "tercih",
    "tereyağı",
    "terlik",
    "termometre",
    "ters",
    "terzi",
    "tesisatçı",
    "test",
    "testere",
    "teyp",
    "teyze",
    "tez",
    "tezgah",
    "ticaret",
    "tilki",
    "timsah",
    "tişört",
    "titiz",
    "tiyatro",
    "tiyatrocu",
    "tıkaç",
    "tıpkı",
    "tır",
    "tırmık",
    "tırnak",
    "tırtıl",
    "tohum",
    "tok",
    "toka",
    "ton",
    "top",
    "topaç",
    "toplam",
    "toplama",
    "topluiğne",
    "toplum",
    "toprak",
    "topuk",
    "topuz",
    "torba",
    "tören",
    "tornavida",
    "törpü",
    "torun",
    "tost",
    "toz",
    "trafik",
    "trajedi",
    "traktör",
    "trampet",
    "tramvay",
    "tren",
    "trompet",
    "tüccar",
    "tuğla",
    "tüketici",
    "tüketim",
    "tül",
    "tüm",
    "tünel",
    "tüp",
    "tür",
    "turist",
    "turizm",
    "türkü",
    "turnuva",
    "turp",
    "turşu",
    "turta",
    "turuncu",
    "tuş",
    "tutam",
    "tutar",
    "tutkal",
    "tütsü",
    "tutumlu",
    "tuvalet",
    "tüy",
    "tuz",
    "tuzak",
    "tuzluk",
    "uç",
    "üç",
    "uçak",
    "üçgen",
    "ücret",
    "üçüncü",
    "uçurtma",
    "uçurum",
    "uçuş",
    "ucuz",
    "üçüz",
    "ufak",
    "uğra",
    "uğraş",
    "uğur",
    "uğurlu",
    "ülke",
    "ulu",
    "ulus",
    "ulusal",
    "ümit",
    "umut",
    "un",
    "ün",
    "ünite",
    "ünlem",
    "ünlü",
    "üretim",
    "ürün",
    "uşak",
    "uslu",
    "üst",
    "usta",
    "utangaç",
    "ütü",
    "üvey",
    "uyak",
    "uyanık",
    "uydu",
    "üye",
    "uyku",
    "uyluk",
    "uzak",
    "uzay",
    "üzeri",
    "üzgün",
    "uzman",
    "üzüm",
    "uzun",
    "uzunluk",
    "üzüntü",
    "vadi",
    "vagon",
    "vahşi",
    "vakit",
    "vali",
    "valiz",
    "vapur",
    "var",
    "varlık",
    "vaşak",
    "vatan",
    "vatandaş",
    "vazo",
    "ve",
    "veli",
    "vergi",
    "verimli",
    "veteriner",
    "vida",
    "video",
    "villa",
    "vinç",
    "viraj",
    "virüs",
    "vişne",
    "vitamin",
    "vites",
    "vitrin",
    "vize",
    "voleybol",
    "volkan",
    "vücut",
    "yağ",
    "yağmur",
    "yağmurlu",
    "yağmurluk",
    "yaka",
    "yakın",
    "yakışıklı",
    "yakıt",
    "yalan",
    "yalı",
    "yalnız",
    "yama",
    "yamaç",
    "yamuk",
    "yan",
    "yanak",
    "yanardağ",
    "yangın",
    "yanık",
    "yanıt",
    "yankı",
    "yanlış",
    "yanlışlık",
    "yapboz",
    "yapı",
    "yapıştırıcı",
    "yaprak",
    "yara",
    "yaralı",
    "yaramaz",
    "yarar",
    "yararlı",
    "yarasa",
    "yaratıcılık",
    "yaratık",
    "yardım",
    "yardımcı",
    "yarım",
    "yarın",
    "yarış",
    "yarışma",
    "yaş",
    "yasa",
    "yasak",
    "yasal",
    "yaşam",
    "yasemin",
    "yaşlı",
    "yastık",
    "yat",
    "yatak",
    "yavaş",
    "yavru",
    "yay",
    "yaya",
    "yaygın",
    "yayla",
    "yaz",
    "yazar",
    "yazı",
    "yazıcı",
    "yazık",
    "yazılı",
    "yazlık",
    "ye",
    "yedek",
    "yedi",
    "yedinci",
    "yeğen",
    "yel",
    "yelek",
    "yelken",
    "yelkenli",
    "yelpaze",
    "yem",
    "yemek",
    "yemin",
    "yen",
    "yenge",
    "yengeç",
    "yeni",
    "yeniden",
    "yenik",
    "yenilgi",
    "yer",
    "yeşil",
    "yeşillik",
    "yetenek",
    "yetenekli",
    "yeter",
    "yetişkin",
    "yetmiş",
    "yiğit",
    "yine",
    "yirmi",
    "yiyecek",
    "yığın",
    "yıl",
    "yılan",
    "yılbaşı",
    "yıldırım",
    "yıldız",
    "yıllık",
    "yırtık",
    "yoga",
    "yoğun",
    "yoğurt",
    "yoklama",
    "yokuş",
    "yol",
    "yolcu",
    "yolculuk",
    "yön",
    "yonca",
    "yönerge",
    "yönetici",
    "yönetim",
    "yönetişim",
    "yönetmelik",
    "yönetmen",
    "yöntem",
    "yöre",
    "yöresel",
    "yorgan",
    "yorgun",
    "yorum",
    "yörünge",
    "yosun",
    "yüce",
    "yudum",
    "yufka",
    "yük",
    "yukarı",
    "yüklenici",
    "yüklü",
    "yüksek",
    "yulaf",
    "yumak",
    "yumruk",
    "yumurta",
    "yumurtalık",
    "yumuşak",
    "yün",
    "yunus",
    "yürek",
    "yurt",
    "yurtsever",
    "yurttaş",
    "yürüyüş",
    "yuva",
    "yuvarlak",
    "yüz",
    "yüzük",
    "yüzyıl",
    "zafer",
    "zaman",
    "zamir",
    "zar",
    "zarar",
    "zarf",
    "zarif",
    "zaten",
    "zavallı",
    "zebra",
    "zehir",
    "zeka",
    "zeki",
    "zemin",
    "zevk",
    "zeytin",
    "zil",
    "zincir",
    "ziyafet",
    "ziyaret",
    "zımba",
    "zırh",
    "zıt",
    "zombi",
    "zor",
    "zorluk",
    "zorunlu",
    "zürafa",
    "zurna"
]