# Word Ladders

This is the word ladder game in Turkish with a Turkish word list.
This game can be have research or practical (i.e. teaching or therapy) applications.
It can even be used as a fun activity to do that will strengthen ones cognitive and linguistic abilities.
This activity will be beneficial especially for children who are learning to read.
In this game you build a ladder of words.
Each step is a different word that is different from the previous one by a single letter. 
For example: cat bat bar bear

Live website: https://theoddler.gitlab.io/word-ladder-js/

## Attribution

Original code by https://gitlab.com/wangtianxia-sjtu/word-ladder-js-benchmark
